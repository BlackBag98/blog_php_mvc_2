document.addEventListener("keypress",function(e){
	if(e.keyCode === 13){
		e.preventDefault();
	}
});


if(document.querySelector("#email")){

	const email = document.querySelector("#email");
	const btnEmail = document.querySelector("#btnEmail");
	let validEmail = false;

	btnEmail.addEventListener("click",function(e){
		email.blur();
		if(!validEmail){
			e.preventDefault();
		}
	});

	email.addEventListener("blur",function(e){
		if(this.value == '' || this.length == 0 || !(/^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/.test(this.value)) ){
			validEmail = false;
		}else{
			validEmail = true;
		}
	});
}


if(document.querySelector("#nPassword")){

	const nPassword = document.querySelector("#nPassword");
	const confNPasswrod = document.querySelector("#confNPassword");
	const btnNPassword = document.querySelector("#btnNPassword");

	let validPassword = false;
	let validConfPassword = false;

	btnNPassword.addEventListener("click",function(e){
		nPassword.blur();
		confNPassword.blur();
		if(!validPassword || !validConfPassword){
			e.preventDefault();
		}
	});

	nPassword.addEventListener("blur",function(e){
		if(this.value == '' || this.length == 0 || !(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}/.test(this.value)) ){
			validPassword = false;
		}else{
			validPassword = true;
		}
	});

	confNPassword.addEventListener("blur",function(e){
		if(this.value === nPassword.value){
			validConfPassword = true;
		}else{
			validConfPassword = false;
		}
	});
}