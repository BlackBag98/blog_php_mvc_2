<?php

if(isset($_SESSION)){
	if(!empty($_SESSION['member'])){
		$member=unserialize($_SESSION['member']);
		try{
			$subscriptions = Subscription::getAllSubscriptionsByAuthorId($member->getId());
		}catch(UnavailableElementException $e){
			$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
		}

		try{
			$mySubscriptions = Subscription::getAllSubscriptionsBySubscriberId($member->getId());
		}catch(UnavailableElementException $e){
			$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
		}

		try{
			$views = View::getViewsByAuthorId($member->getId());
		}catch(UnavailableElementException $e){
			$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
		}

		try{
			$articles = Article::getAuthorArticles($member->getId());
		}catch(UnavailableElementException $e){
			$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
		}

		try{
			$comments = Comment::getAllCommentsByAuthorId($member->getId());
		}catch(UnavailableElementException $e){
			$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
		}

		$score = 0;
		if(isset($comments)){
			foreach ($comments as $comment) {
				$score += $comment->getScore();
			}
		}	

		$countAllViews = 0;
		if(isset($articles)){
			foreach ($articles as $article) {
				try{
					$articleViews=View::getViewsByArticleId($article->getId());
				}catch(UnavailableElementException $e){
					$articleViews=NULL;
					$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
				}
				$countAllViews += (isset($articleViews)&&!empty($articleViews) ? $articleViews->count():0 );
			}
		}

		try{
			$pageMember = new Member();
			$pageMember->setMemberById($member->getId());
		}catch(UnavailableElementException $e){
			$errorStack='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
		}

		try{
			$role = new Privilege();
			$role->setPrivilegeById($pageMember->getPrivilegeId());
		}catch(UnavailableElementException $e){
			$errorStack='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
		}


		if(isset($_POST['subscribeAuthor'])&&isset($_SESSION['member'])){
			$member = unserialize($_SESSION['member']);
			$subscription = new Subscription();
			try{
				$subscription->setSubscriptionBySubscriberAndAuthorId($member->getId(),$member->getId());
			}catch(UnavailableElementException $e){
				$subscription = new SubscriptionBuilder((int)$member->getId(),$member->getId(),$member->getEmail());
				$subscription->add();
			}
			header('Location: /monCompte');
		}


		if(isset($_POST['unsubscribeAuthor'])&&isset($_SESSION['member'])){
			$member = unserialize($_SESSION['member']);
			$subscription = new Subscription();
			try{
				if(isset($_POST['authorId'])){
					$subscription->setSubscriptionBySubscriberAndAuthorId($member->getId(),$_POST['authorId']);
				}else{
					$subscription->setSubscriptionBySubscriberAndAuthorId($member->getId(),$member->getId());
				}
				$subscription->delete();
			}catch(UnavailableElementException $e){
				echo $e->getMessage();
			}
			header('Location: /monCompte');
		}


		if(isset($_POST['toggleDisableAlert'])&&isset($_SESSION['member'])){
			$subscription = new Subscription();
			try{
				$subscription->setSubscriptionBySubscriberAndAuthorId($member->getId(),$_POST['authorId']);
				$subscription->toggleAlert();
			}catch(UnavailableElementException $e){
				echo $e->getMessage();
			}
			header('Location: /monCompte');
		}

						if(isset($_POST['plusComment'])){
							# code...
							$comment = new Comment();
							$comment->setCommentById($_POST['commentId']);
							$comment->setScore($comment->getScore()+1);
							$comment->update();
							header('Location: /monCompte');
						}
						if(isset($_POST['moinsComment'])) {
							# code...
							$comment = new Comment();
							$comment->setCommentById($_POST['commentId']);
							$comment->setScore($comment->getScore()-1);
							$comment->update();
							header('Location: /monCompte');							
						}
						if(isset($_POST['reportComment'])) {
							# code...
							$comment = new Comment();
							$comment->setCommentById($_POST['commentId']);
							$comment->setReportings($comment->getReportings()+1);
							$comment->update();
							header('Location: /monCompte');							
						}

						if(isset($_POST['deleteComment'])&&isset($_SESSION['member'])){
							$member=unserialize($_SESSION['member']);
							$comment=new Comment();
							$comment->setCommentById($_POST['commentId']);
							if($comment->getAuthorId()==$member->getId()){
								$comment->delete();
								header('Location: /monCompte');
							}
						}

						if(isset($_POST['modifyComment'])&&isset($_SESSION['member'])){
							$member=unserialize($_SESSION['member']);
							$comment=new Comment();
							$comment->setCommentById($_POST['commentId']);
							if($comment->getAuthorId()==$member->getId()){
								header('Location: /article?action=modifyComment&commentId='.$comment->getId());
							}
						}	
	}else{$errorStack[]='Error in '.__FILE__.' Vous n\'etes pas connecte.';}
}
?>