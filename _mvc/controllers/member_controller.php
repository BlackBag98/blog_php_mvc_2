<?php

if(isset($_GET)){
	if(!empty($_GET['id'])&&is_numeric($_GET['id'])){
		try{
			$subscriptions = Subscription::getAllSubscriptionsByAuthorId($_GET['id']);
		}catch(UnavailableElementException $e){
			$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
		}

		try{
			$views = View::getViewsByAuthorId($_GET['id']);
		}catch(UnavailableElementException $e){
			$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
		}

		try{
			$articles = Article::getAuthorArticles($_GET['id']);
		}catch(UnavailableElementException $e){
			$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
		}

		try{
			$comments = Comment::getAllCommentsByAuthorId($_GET['id']);
		}catch(UnavailableElementException $e){
			$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
		}

		$score = 0;
		if(isset($comments)){
			foreach ($comments as $comment) {
				$score += $comment->getScore();
			}
		}	
		$countAllViews = 0;
		if(isset($articles)){
			foreach ($articles as $article) {
				try{
					$articleViews=View::getViewsByArticleId($article->getId());
				}catch(UnavailableElementException $e){
					$articleViews=NULL;
					$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
				}
				$countAllViews += (isset($articleViews)&&!empty($articleViews) ? $articleViews->count():0 );
			}
		}

		try{
			$pageMember = new Member();
			$pageMember->setMemberById($_GET['id']);
		}catch(UnavailableElementException $e){
			$errorStack='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
		}

		try{
			$role = new Privilege();
			$role->setPrivilegeById($pageMember->getPrivilegeId());
		}catch(UnavailableElementException $e){
			$errorStack='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
		}


		if(isset($_POST['subscribeAuthor'])&&isset($_SESSION['member'])){
			$member = unserialize($_SESSION['member']);
			$subscription = new Subscription();
			try{
				$subscription->setSubscriptionBySubscriberAndAuthorId($member->getId(),$_GET['id']);
			}catch(UnavailableElementException $e){
				$subscription = new SubscriptionBuilder((int)$_GET['id'],$member->getId(),$member->getEmail());
				$subscription->add();
			}
			header('Location: /member?name='.strtolower($pageMember->getFirstname()).'_'.strtolower($pageMember->getLastname()).'&id='.$pageMember->getId());
		}


		if(isset($_POST['unsubscribeAuthor'])&&isset($_SESSION['member'])){
			$member = unserialize($_SESSION['member']);
			$subscription = new Subscription();
			try{
				$subscription->setSubscriptionBySubscriberAndAuthorId($member->getId(),$_GET['id']);
				$subscription->delete();
			}catch(UnavailableElementException $e){
				echo $e->getMessage();
			}
			header('Location: /member?name='.strtolower($pageMember->getFirstname()).'_'.strtolower($pageMember->getLastname()).'&id='.$pageMember->getId());			
		}
	}else{$errorStack[]='Error in '.__FILE__.' GET value invalid or not found';}
}
?>