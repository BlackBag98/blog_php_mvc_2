<?php
$affichage=0;
if(isset($_GET)){

		if(isset($_GET['action'])){

			if($_GET['action']=='read'){
				if(!empty($_GET['id'])&&is_numeric($_GET['id']))
				{
					$affichage=1;
					$currentArticle = new Article();
					if(isset($_SESSION['member'])){
						$member = unserialize($_SESSION['member']);
					}

						try{ 
							$currentArticle->setArticleById($_GET['id']);
						}catch(UnavailableElementException $e){
							$errorStack[]='Error in FILE: '.$e->getTrace()[0]['file'].' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
						}

						try{
							$comments = Comment::getAllCommentsByArticleId($currentArticle->getId());
							#debug($currentArticle);
							#debug($comments);
						}catch(UnavailableElementException $e){
							$errorStack[]='Error in FILE: '.$e->getTrace()[0]['file'].' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
						}

						try{
							$views = View::getViewsByArticleId($currentArticle->getId());
						}catch(UnavailableElementException $e){
							$errorStack[]='Error in FILE: '.$e->getTrace()[0]['file'].' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
						}

						/*
						try{
							$ratings = View::getViewsByArticleId($currentArticle->getId());
						}catch(UnavailableElementException $e){
							$errorStack[]='Error in FILE: '.$e->getTrace()[0]['file'].' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
						}

						try{
							$images = View::getViewsByArticleId($currentArticle->getId());
						}catch(UnavailableElementException $e){
							$errorStack[]='Error in FILE: '.$e->getTrace()[0]['file'].' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
						}

						try{
							$files = View::getViewsByArticleId($currentArticle->getId());
						}catch(UnavailableElementException $e){
							$errorStack[]='Error in FILE: '.$e->getTrace()[0]['file'].' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
						}
						*/
						if(isset($_POST['modifyArticle'])&&isset($member)&&($currentArticle->getAuthorId()==$member->getId())){
							# code...
							header('Location: /article?action=modify&title='.join('-',explode(' ',strtolower($currentArticle->getTitle()))).'&id='.$currentArticle->getId());
						}
						if(isset($_POST['deleteArticle'])&&isset($member)&&($currentArticle->getAuthorId()==$member->getId())){
							# code...
							$currentArticle->deleteArticle();
							header("Location: /");
							exit();
						}
						if(isset($_POST['reportArticle'])){
							# code...
							$currentArticle->reportArticle();
						}
						if(isset($_POST['noteArticle'])&&isset($member)) {
							# code...
							$rating = new RatingBuilder();
							$rating->setArticleId($currentArticle->getId());
							$rating->setAuthorId($member->getId());
							$rating->setRating($_POST['noteArticle']);
							try{
								$rating->add();
								$message='<div class="container"><div class="alert alert-success alert-dismissible col-12" ><button type="bubtton" class="close"><span aria-hidden="true>&times;</span></button>Votre note a ete prise en compte merci.</div></div>"';
							}catch(Exception $e){
								$errorStack[]='Error in FILE: '.$e->getTrace()[0]['file'].' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
								$message='<div class="container"><div class="alert alert-success alert-dismissible col-12" ><button type="bubtton" class="close"><span aria-hidden="true>&times;</span></button>Erreur de validation de note.</div></div>"';
							}
						}
						if(isset($_POST['toggleDisableComment'])&&isset($member)&&($currentArticle->getAuthorId()==$member->getId())){
							# code...
							$currentArticle->setDisableComments(!$currentArticle->getDisableComments());
							$currentArticle->update();
							header('Location: /article?action=read&title='.join('-',explode(' ',strtolower($currentArticle->getTitle()))).'&id='.$currentArticle->getId());
						}
						if(isset($_POST['commentArticle'])&&isset($member)){
							# code...
							$comment = new CommentBuilder();
							$comment->setArticleId($currentArticle->getId());
							$comment->setAuthorId($member->getId());
							$comment->setResponseTo(null);
							$comment->setContent($_POST['content']);
							$comment->add();
							header('Location: /article?action=read&title='.join('-',explode(' ',strtolower($currentArticle->getTitle()))).'&id='.$currentArticle->getId());
						}
						if(isset($_POST['responseComment'])&&isset($member)){
							# code...
							$comment = new CommentBuilder();
							$comment->setArticleId($currentArticle->getId());
							$comment->setAuthorId($member->getId());
							$comment->setResponseTo($_POST['parentCommentId']);
							$comment->setContent($_POST['content']);
							$comment->add();
							header('Location: /article?action=read&title='.join('-',explode(' ',strtolower($currentArticle->getTitle()))).'&id='.$currentArticle->getId());
						}
						if(isset($_POST['plusComment'])){
							# code...
							$comment = new Comment();
							$comment->setCommentById($_POST['commentId']);
							$comment->setScore($comment->getScore()+1);
							$comment->update();
							header('Location: /article?action=read&title='.join('-',explode(' ',strtolower($currentArticle->getTitle()))).'&id='.$currentArticle->getId());
						}
						if(isset($_POST['moinsComment'])) {
							# code...
							$comment = new Comment();
							$comment->setCommentById($_POST['commentId']);
							$comment->setScore($comment->getScore()-1);
							$comment->update();
							header('Location: /article?action=read&title='.join('-',explode(' ',strtolower($currentArticle->getTitle()))).'&id='.$currentArticle->getId());							
						}
						if(isset($_POST['reportComment'])) {
							# code...
							$comment = new Comment();
							$comment->setCommentById($_POST['commentId']);
							$comment->setReportings($comment->getReportings()+1);
							$comment->update();
							header('Location: /article?action=read&title='.join('-',explode(' ',strtolower($currentArticle->getTitle()))).'&id='.$currentArticle->getId());							
						}

						if(isset($_POST['deleteComment'])&&isset($_SESSION['member'])){
							$member=unserialize($_SESSION['member']);
							$comment=new Comment();
							$comment->setCommentById($_POST['commentId']);
							if($comment->getAuthorId()==$member->getId()){
								$comment->delete();
								header('Location: /article?action=read&title='.join('-',explode(' ',strtolower($currentArticle->getTitle()))).'&id='.$currentArticle->getId());
							}
						}

						if(isset($_POST['modifyComment'])&&isset($_SESSION['member'])){
							$member=unserialize($_SESSION['member']);
							$comment=new Comment();
							$comment->setCommentById($_POST['commentId']);
							if($comment->getAuthorId()==$member->getId()){
								header('Location: /article?action=modifyComment&commentId='.$comment->getId());
							}
						}	

						if(isset($_POST['subscribeAuthor'])&&isset($_SESSION['member'])){
							$member = unserialize($_SESSION['member']);
							$subscription = new Subscription();
							try{
								$subscription->setSubscriptionBySubscriberAndAuthorId($member->getId(),$currentArticle->getAuthorId());
							}catch(UnavailableElementException $e){
								$subscription = new SubscriptionBuilder((int)$currentArticle->getAuthorId(),$member->getId(),$member->getEmail());
								$subscription->add();
							}
							header('Location: /article?action=read&title='.join('-',explode(' ',strtolower($currentArticle->getTitle()))).'&id='.$currentArticle->getId());							
						}


						if(isset($_POST['unsubscribeAuthor'])&&isset($_SESSION['member'])){
							$member = unserialize($_SESSION['member']);
							$subscription = new Subscription();
							try{
								$subscription->setSubscriptionBySubscriberAndAuthorId($member->getId(),$currentArticle->getAuthorId());
								$subscription->delete();
							}catch(UnavailableElementException $e){
								echo $e->getMessage();
							}
							header('Location: /article?action=read&title='.join('-',explode(' ',strtolower($currentArticle->getTitle()))).'&id='.$currentArticle->getId());							
						}
				}



			}elseif($_GET['action']=='writeArticle'){

				if(isset($_SESSION)) {
					
					if(isset($_SESSION['member'])){
						$affichage=2;
						if(isset($_POST)){
							if(isset($_POST['btnAddDownloadArticle'])){

							}
							if(isset($_POST['btnAddImageArticle'])){

							}
							if(isset($_POST['btnWriteArticle'])){
								
								$member=unserialize($_SESSION['member']);
								$newArticle = new ArticleBuilder();

								$newArticle->setTitle($_POST['title']);
								$newArticle->setSentence($_POST['sentence']);
								$newArticle->setContent($_POST['content']);
								$newArticle->setAuthorId($member->getId());
								$newArticle->setCategoryId($_POST['category']);
								try{
									$newArticle->add();

									$lastArticle = new Article();
									$lastArticle->setLastArticle();
									#$message= '<div class="container"><div class="alert alert-success alert-dismissible col-12"><button type="button" class="close"><span aria-hidden="true">&times;</span></button> Votre article a ete creer.<a href="http://localhost/article?action=read&title='.join('-',explode(' ',strtolower($_POST['title']))).'&id='.($lastArticle->getId()+1).'"> >>ICI<< </a></div></div>';
									try{
										$subscribers = Subscription::getAllSubscriptionsByAuthorId($member->getId());
									}catch(UnavailableElementException $e){
										$subscribers=NULL;
									}
									if(!empty($subscribers)){
										foreach ($subscribers as $subscriber) {
											$message='(Pour desactiver les notifications connecter vous a l\'espace monCompte)<br>Voici le lien de l\'article : <a href="https://www.prufung.online/article?action=read&title='.join('-',explode(' ',strtolower($_POST['title']))).'&id='.($lastArticle->getId()).'">Lien vers article.</a>';
                                            if($subscriber->getAlert()){
    											mail($subscriber->getSubscriberEmail(), 'Notification '.ucfirst($member->getFirstname()).' '.strtoupper($member->getLastname()).' a ecrit un article.', $message);
                                            }
										}
									}
									header('Location: /article?action=read&title='.join('-',explode(' ',strtolower($_POST['title']))).'&id='.($lastArticle->getId()));
									exit();
								}catch(Exception $e){
									$errorStack[]='Error in FILE: '.$e->getTrace()[0]['file'].' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
									$message= '<div class="container"><div class="alert alert-danger alert-dismissible col-12"><button type="button" class="close"><span aria-hidden="true">&times;</span></button> Erreur de validation .</div></div>';
								}
							}
						}
					}else{$affichage=3;$message='<div class="container"><div class="alert alert-danger alert-dismissible"><button type="button" class="close"><span aria-hidden="true">&times;</span></button> Vous devez etre connecter pour ce type d\'action</div></div>';}
				}else{$affichage=3;$message='<div class="container"><div class="alert alert-danger alert-dismissible"><button type="button" class="close"><span aria-hidden="true">&times;</span></button> Vous devez etre connecter pour ce type d\'action</div></div>';}
			
			}elseif($_GET['action']=='modify'){
				if(isset($_SESSION)){
					if(isset($_SESSION['member'])){
						$affichage=4;
						$currentArticle=new Article();
						$member=unserialize($_SESSION['member']);
						try{ 
							$currentArticle->setArticleById($_GET['id']);
						}catch(UnavailableElementException $e){
							$errorStack[]='Error in FILE: '.$e->getTrace()[0]['file'].' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
						}
						if($currentArticle->getAuthorId()==$member->getId()){
							if(isset($_POST['btnModifyArticle'])){
								$currentArticle->setTitle($_POST['title']);
								$currentArticle->setSentence($_POST['sentence']);
								$currentArticle->setContent($_POST['content']);
								$currentArticle->setAuthorId($member->getId());
								$currentArticle->setCategoryId($_POST['category']);
								if($currentArticle->update()){
									header('Location: /article?action=read&title='.join('-',explode(' ',strtolower($currentArticle->getTitle()))).'&id='.$currentArticle->getId());							
								}else{$affichage=3;$message='<div class="container"><div class="alert alert-danger alert-dismissible"><button type="button" class="close"><span aria-hidden="true">&times;</span></button> Probleme de validation des modifications</div></div>';}
							}
						}else{$affichage=3;$message='<div class="container"><div class="alert alert-danger alert-dismissible"><button type="button" class="close"><span aria-hidden="true">&times;</span></button> Cette article ne vous appartient pas!</div></div>';}
					}else{$affichage=3;$message='<div class="container"><div class="alert alert-danger alert-dismissible"><button type="button" class="close"><span aria-hidden="true">&times;</span></button> Vous devez etre connecter pour ce type d\'action</div></div>';}
				}else{$affichage=3;$message='<div class="container"><div class="alert alert-danger alert-dismissible"><button type="button" class="close"><span aria-hidden="true">&times;</span></button> Vous devez etre connecter pour ce type d\'action</div></div>';}

			}elseif($_GET['action']=='modifyComment'){
				if($_SESSION['member']){
					$affichage=5;
					$member=unserialize($_SESSION['member']);
					$comment=new Comment();
					$comment->setCommentById($_GET['commentId']);
					$currentArticle=new Article();
					$currentArticle->setArticleById($comment->getArticleId());
					if($comment->getAuthorId()==$member->getId()){
						if(isset($_POST['btnModifyComment'])){
							$comment->setContent($_POST['content']);
							if($comment->update()){
								header('Location: /article?action=read&title='.join('-',explode(' ',strtolower($currentArticle->getTitle()))).'&id='.$currentArticle->getId());							
							}else{$affichage=3;$message='<div class="container"><div class="alert alert-danger alert-dismissible"><button type="button" class="close"><span aria-hidden="true">&times;</span></button> Probleme de validation des modification</div></div>';}
						}
					}else{$affichage=3;$message='<div class="container"><div class="alert alert-danger alert-dismissible"><button type="button" class="close"><span aria-hidden="true">&times;</span></button> Ce commentaire ne vous appartient pas!</div></div>';}
				}else{$affichage=3;$message='<div class="container"><div class="alert alert-danger alert-dismissible"><button type="button" class="close"><span aria-hidden="true">&times;</span></button> Vous devez etre connecter pour ce type d\'action</div></div>';}

			}else{$errorStack[]='Error in '.__FILE__.' GET value invalid or not found';}
		}else{$errorStack[]='Error in '.__FILE__.' GET value invalid or not found';}
}

?>