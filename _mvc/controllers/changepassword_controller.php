<?php

if(isset($_GET["hash"])&&!empty($_GET["hash"])){
	$hash = new Hash();
	try{
		$hash->setHashByHash($_GET['hash']);
		$affichage = 1;
	}catch(UnavailableElementException $e){$error = "Ce hash de reintialisation de mot de passe n'est pas valide";}
}

if(isset($_POST)){
	if(isset($_POST['btnNewPassword'])){
		if(isset($_POST['newPassword'])&&!empty($_POST['newPassword'])){
			$newPassword = $_POST['newPassword'];
			if(isset($_POST['confNewPassword'])&&!empty($_POST['confNewPassword'])){
				$confNewPassword = $_POST['confNewPassword'];
				if(isset($_POST['hash'])&&!empty($_POST['hash'])){
					$rawHash = $_POST['hash'];
					$hash = new Hash();
					try{
						$hash->setHashByHash($rawHash);
						if(!$hash->getObselete()){
							if(!$hash->Timeout()){
								if($hash->getPurpose()=='password_change')
									$member=new Member(); 
									try{
										$member->setMemberByEmail($hash->getEmail());
										if(preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}#', $newPassword)){
											if($newPassword===$confNewPassword){
												$member->changePassword($newPassword);
												$hash->delete();
												$success = "Votre mot de passe a bien etait modifie.";
											}else{$error = "Le mot de passe de confirmation est invalide";}
										}else{$error = "Le mot de passe est invalide veuillez respecter la syntaxe.";}
									}catch(UnavailableElementException $e){
										$error = "Ce hash n'est relie a aucun compte.";
									}
							}else{$error = "Le hash de reintialisation de mot de passe a expire (15mins).";}
						}else{$error = "Le hash de reintialisation de mot de passe est obselete.";}
						#}else{$error = "";}
					}catch(UnavailableElementException $e){
						$error = "Le hash de reintialisation de mot de passe n'est pas valide.";
					}
				}else{$error = "Le hash de reintialisation de mot de passe est vide.";}
			}else{$error = "Le mot de passe de confirmation est vide.";}
		}else{$error = "Le mot de passe est vide.";}
	}else{$error = "Le boutton ne correspond pas au formulaire.";}
}else{$error = "Aucun formulaire valide.";}

if(isset($_GET["btnSendEmailNewPassword"])){
	if(isset($_GET["emailNewPassword"])&&!empty($_GET["emailNewPassword"])) {
		$email = strtolower($_GET['emailNewPassword']);
		if(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#', $email) ){
			$member = new Member();
			try{
				$member->setMemberByEmail($email);

				$door = false;
				$oldHash=new Hash();
				try{
					$oldHash->setHashByEmail($email);
				}catch(UnavailableElementException $e){
					$door=true;
				}
				#$debug=debug($oldHash);
				if($door||$oldHash->delete()){
					$hash = new Hash();
					$rawHash = randHash();
					$newHash=new HashBuilder();
					while (!empty($hash)) {
						try{
							$hash=new Hash();
							$hash->setHashByHash($rawHash);
							$rawHash = randHash();
						}catch(UnavailableElementException $e){
							$hash=NULL;
							$newHash->setEmail($email);
							$newHash->setHash($rawHash);
							$newHash->setPurpose('password_change');
							$newHash->setMinutes(15);
						}
					}
					if($newHash->add()){

						$lien = "https://prufung.online/changepassword?hash=".$rawHash;
						$message = "Voici le lien de reinitialisation de mot de passe : ".$lien;
						mail($email, "Lien reinitialisation de mot de passe (Ce lien dure 15 mins)", $message);
						$success="Un email de reinitialisation de mot de passe a etait envoye a ".$email." (Ce lien dure 15 mins)";

					}else{$error = "Erreur lors de la creation du nouveau lien de reintialisation de mot de passe.";}
				}else{$error = "Erreur lors de la creation du nouveau lien de reintialisation de mot de passe.";}
			}catch(UnavailableElementException $e){$error="Cette email n'est reliee a aucun compte";}
		}else{$error = "L'email n'est pas valide";}
	}else{$error = "Aucun email, veuillez ecrire votre email";}
}



?>