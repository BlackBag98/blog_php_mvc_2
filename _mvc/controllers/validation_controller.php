<?php


if(isset($_GET)&&isset($_GET['hash'])){
	$hash = new Hash();
	try{
		$hash->setHashByHash($_GET['hash']);
	}catch(UnavailableElementException $e){
		$hash=NULL;
	}
	if(!empty($hash)){
		if(!$hash->getObselete()){
			if(!$hash->Timeout()){
				if($hash->getPurpose()=='account_validation'){
					$hashMember = new Member();
					try{
						$hashMember->setMemberByEmail($hash->getEmail());
					}catch(UnavailableElementException $e){
						$hashMember=NULL;
					}
					if(!empty($hashMember)){
						$hashMember->enableAccountActivation();
						$hash->delete();
						$validation_success = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>Votre compte est maintenant actif. Bienvenue '.ucfirst($hashMember->getFirstname()).' '.strtoupper($hashMember->getLastname()).'</div>';
					}else{$validation_error='Aucun compte n\'est relie a ce lien d\'activation';}
				}else{$validation_error='Ce lien n\'est pas un lien d\'activation de compte';}
			}else{$validation_error='Le lien d\'activation a expirer (un lien dure 15 mins)';}
		}else{$validation_error='Le lien d\'activation est desactiver.';}
	}else{$validation_error='Le hash d\'activation de votre lien est invalide.';}
}elseif(isset($_POST['btnSendNewActivationLink'])){
	if(isset($_POST['email'])&&!empty($_POST['email'])){
		if(preg_match('/^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/', strtolower($_POST['email']))){
			$hashMember = new Member();
			try{
				$hashMember->setMemberByEmail(strtolower($_POST['email']));
			}catch(UnavailableElementException $e){
				$hashMember=NULL;
			}
			if(!empty($hashMember)){
				if(!$hashMember->getAccountActivation()){
					$door = false;
					$oldHash=new Hash();
					try{
						$oldHash->setHashByEmail($hashMember->getEmail());
					}catch(UnavailableElementException $e){
						$door=true;
					}
					if($door==true||$oldHash->delete()){
						$hash = new Hash();
						$rawHash = randHash();
						$newHash=new HashBuilder();
						while (!empty($hash)) {
							try{
								$hash=new Hash();
								$hash->setHashByHash($rawHash);
								$rawHash = randHash();
							}catch(UnavailableElementException $e){
								$hash=NULL;
								$newHash->setEmail(strtolower($_POST['email']));
								$newHash->setHash($rawHash);
								$newHash->setPurpose('account_validation');
								$newHash->setMinutes(15);
							}
						}
						if($newHash->add()){

							$validationLink = '<br><a href="https://www.prufung.online/validation?hash='.$rawHash.'">Lien d\'activation du compte</a>';
							$validation_success = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>Un nouveau email de confirmation vous a été envoyé.</div>';
							$emailMessage = 'Merci de votre inscription : <br>'.ucfirst($hashMember->getFirstname()).' '.strtoupper($hashMember->getLastname()).'<br>Votre identifiant est : '.$_POST['email'].'<br>Votre nouveau lien d\'activation : '.$validationLink;
							mail($_POST['email'], 'Email de confimation d\'inscription', $emailMessage);

						}else{$validation_error = 'Erreur lors de la  creation du nouveau lien d\'activation.';}
					}else{$validation_error = 'Erreur lors de la  creation du nouveau lien d\'activation.';}	
				}else{$validation_error = 'Ce compte est deja actif le lien d\'activation ne peut etre genere.';}
			}else{$validation_error = 'Aucun compte n\'existe avec cette email aucun lien d\'ativation ne peut etre genere.';}
		}else{$validation_error = 'L\'adresse mail n\'est pas valide !';}
	}else{$validation_error="Une erreur c\'est produite lors de la validation du mail !";}
}else{}



?>