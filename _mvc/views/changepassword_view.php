<!DOCTYPE html>
<html>
<?php include_once 'statics/head_view.php';?>
<body>
<?php include_once 'statics/header_view.php';?>
<?php
	
	if(!isset($_GET['hash'])||empty($_GET['hash'])){
		echo 
		'<div class="container mt-5 mb-5 col-md-6 col-lg-6 border rounded p-3 text-center">
			<h1>Mot de passe oublie ?</h1>
			<p>Envoyer un lien re-initialisation de mot de passe.</p>
			<form>
				<div class="form-group text-center">
					<input type="email" class="form-control mb-3" id="email" name="emailNewPassword" placeholder="example@mail.com">
					<input type="submit" class="btn btn-block btn-success m-1" id="btnEmail" name="btnSendEmailNewPassword" value="Envoyer">
				</div>
			</form>';
			if(isset($error)){
				echo $error;
			}
			if (isset($success)) {
				echo $success;
			}
		echo '</div>';
	}

	if(isset($affichage)&&$affichage==1){
		echo
		'<div class="container mt-5 mb-5 col-md-6 col-lg-6 border rounded p-3">
			<div class="text-center">
				<h1>Changer de mot de passe</h1>
				<p>Le mot de passe doit contenir au moins 8 caracteres, une majuscule, une minuscule, un chiffre et un special parmis les suivants "@$!%*?&"</p>
			</div>
			<form method="post">
				<div class="form-group">
					<label for="nPassword">Nouveau mot de passe :</label>
					<input type="password" id="nPassword" class="form-control" name="newPassword">
				</div>
				<div class="form-group">
					<label for="confNPassword">Confirmation mot de passe :</label>
					<input type="password" id="confNPassword" class="form-control" name="confNewPassword">
				</div>
				<br>
				<input type="hidden" id="hash" name="hash" value="'.$_GET['hash'].'">
				<input type="submit" class="btn btn-block btn-primary" id="btnNPassword" name="btnNewPassword" value="Modifier">
			</form>';
			if(isset($error)){
				echo $error;
			}
			if (isset($success)) {
				echo $success;
			}
		echo '</div>';
	}

	if(isset($debug)){
		echo $debug;
	}
?>

<script type="text/javascript" src="_assets/javascript/changepassword.js"></script>
</body>
</html>