<html>
<?php include_once 'statics/head_view.php'; ?>
<body>
<?php include_once 'statics/header_view.php'; ?>
<div class="container mb-5">
	<div class="row">
			<div class="col-md-4 col-sm-12 bg-dark rounded m-1">
				<span class="rounded text-white badge badge-secondary m-2"><img class="rounded m-1 mr-4" src="../_assets/images/default_pp.jpg" width="50px" height="50px"><span class="mr-4" style="font-size:130%;"><?php echo ''.(isset($member)&&!empty($member) ? ''.ucfirst($member->getFirstname()).' '.strtoupper($member->getLastname()):'ERROR' ); ?></span></span>
				<form method="post">
					<?php
						if(isset($_SESSION['member'])){
							$member = unserialize($_SESSION['member']);
							$subscription = new Subscription();
							try{
								$subscription->setSubscriptionBySubscriberAndAuthorId($member->getId(),$member->getId());
								echo '<input type="submit" class="btn btn-danger btn-sm m-1" name="unsubscribeAuthor" value="Se desabonner">';
							}catch(UnavailableElementException $e){
								echo '<input type="submit" class="btn btn-warning btn-sm m-1" name="subscribeAuthor" value="S\'abonner">';
							}
						}else{
							echo '<input type="submit" class="btn btn-warning btn-sm m-1" name="subscribeAuthor" value="S\'abonner">';
						}
					?>
				</form>
				<div class="card bg-secondary my-1">
	                  <h5 class="card-header text-uppercase bg-dark text-white">Informations :</h5>
	                  <div class="card-body">
	                    <h5>
	                    	<span class="badge badge-success m-1">Abonnees : <?php echo ''.(isset($subscriptions)&&!empty($subscriptions) ? $subscriptions->count():'0' ); ?></span>
	                    	<span class="badge badge-dark m-1">Article(s) Ecrit: <?php echo ''.(isset($articles)&&!empty($articles) ? $articles->count():'0' ); ?></span>
	                    	<span class="badge badge-primary m-1">Nombre de Vue(s) sur mes Article(s): <?php echo ''.$countAllViews; ?></span>
	                    	<span class="badge badge-info m-1">Commentaire(s) Ecrit(s): <?php echo ''.(isset($comments)&&!empty($comments) ? $comments->count():'0' ); ?></span>
	                    	<span class="badge badge-danger m-1">Role : <?php echo ''.(isset($role)&&!empty($role) ? ucfirst($role->getName()):'PRIVILEGE_ERROR' ); ?></span>
	                    	<span class="badge badge-warning m-1">Totales Score Commentaire(s): <?php echo ''.(isset($score)&&!empty($score) ? $score :'0' ); ?></span>
	                    </h5>
	                  </div>  
	            </div>
	        </div>




			<div class="col-md-7 col-sm-12 bg-warning">

				<div class="card my-5">
				<h5 class="card-header text-uppercase bg-info text-white text-center">MES ARTICLES</h5>
				 <div style="border:2px solid black;width:100%;height:400px;line-height:3em;overflow:auto;padding:5px;">
				 	
				 	<?php
				 		if(isset($articles)){
					 		foreach ($articles as $article) {
					 			# code...
					 			try{
					 				$articleViews=View::getViewsByArticleId($article->getId());
					 			}catch(UnavailableElementException $e){
					 				$articleViews=NULL;
					 				$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
					 			}

					 			try{
									$articleComments=Comment::getAllCommentsByArticleId($article->getId());
					 			}catch(UnavailableElementException $e){
					 				$articleComments=NULL;
					 				$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
					 			}
								echo 
								'<div class="card my-1">
					                  <h5 class="card-header text-uppercase bg-dark text-white">ARTICLE</h5>
					                  <div class="card-body">
					                    <h5 class="card-title">'.$article->getTitle().'</h5>
					                    <span class="badge badge-primary ml-1">Vues : '.(isset($articleViews)&&!empty($articleViews) ? $articleViews->count():'0' ).'</span>
					                    <span class="badge badge-secondary ml-1">Commentaires : '.(isset($articleComments)&&!empty($articleComments) ? $articleComments->count():'0' ).'</span>
					                    <p class="bg-light col-12 p-2"><span class="badge">Ecrit le '.$article->getDate().'</span></p>
					                    <p class="card-text">'.$article->getSentence().'</p>
					                    <a href="/article?action=read&title='.$article->getTitle().'&amp;id='.$article->getId().'" class="btn btn-secondary">Voir plus »</a>
					                  </div>  
					            </div>';
					        }
					    }else{
					    	echo '<h1 class="text-center">Aucun Article(s)</h1>';
					    }
				    ?>

				 </div>
				</div> 
				<div class="card my-5">
				<h5 class="card-header text-uppercase bg-info text-white text-center">MES ABONNEMENTS</h5>
				 <div style="border:2px solid black;width:100%;height:400px;line-height:3em;overflow:auto;padding:5px;">
				 	
				 	<?php
				 		if(isset($mySubscriptions)){
					 		foreach ($mySubscriptions as $mySubscription){
					 			$memberAuthor = new Member();
					 			$memberAuthor->setMemberById($mySubscription->getAuthorId());
								echo 
								'<div class="card my-1">
					                  <h5 class="card-header text-uppercase bg-dark text-white">ABONNEMENT</h5>
					                  <div class="card-body">
					                  	<form name="toggleCommentForm" id="switchComment'.$mySubscription->getId().'" method="post">
					                    	<p class="bg-light col-12 p-2"><span class="badge"><img class="rounded m-1 mr-4" src="../_assets/images/default_pp.jpg" width="50px" height="50px"><a href="/member?name='.$memberAuthor->getFirstname().'_'.$memberAuthor->getLastname().'&id='.$memberAuthor->getId().'">'.ucfirst($memberAuthor->getFirstname()).' '.strtoupper($memberAuthor->getLastname()).'</a></span></p>

									    	<div class="custom-control custom-switch">
											  <input type="hidden" name="authorId" value="'.$memberAuthor->getId().'">
											  <input type="hidden" name="toggleDisableAlert" value="run">
								              <input type="checkbox" class="custom-control-input" name="toggleAlert" id="customSwitch'.$mySubscription->getId().'" '.($mySubscription->getAlert() ? 'checked' : 'unchecked').'>
								              <label class="custom-control-label" for="customSwitch'.$mySubscription->getId().'" onclick="document.getElementById(\'switchComment'.$mySubscription->getId().'\').submit();">Notification (Vous envoie un email lorsque l\'auteur ajoute un article)</label>
								            </div>

								            <input type="submit" class="btn btn-danger btn-sm m-1" name="unsubscribeAuthor" value="Se desabonner">
				            			</form>	
					                  </div>  
					            </div>';
					        }
					    }else{
					    	echo '<h1 class="text-center">Aucun Abonnement(s)</h1>';
					    }
				    ?>

				 </div>
				</div>
				<div class="card my-5">
				<h5 class="card-header text-uppercase bg-info text-white text-center">MES COMMENTAIRES</h5>
				 <div style="border:2px solid black;width:100%;height:400px;line-height:3em;overflow:auto;padding:5px;">
				 	
				 	<?php
				 		if(isset($comments)&&$comments!=null){

				 			$commentAuthor = new Member();
					 		foreach ($comments as $comment) {
					        		
									try{
										$commentResponses = Comment::getResponsesForCommentId($comment->getId());
						
									}catch(Exception $e){
										$errorStack[]='Error in '.__FILE__.' '.$e->getMessage();
									}

						        	try{
							      		$commentAuthor->setMemberById($comment->getAuthorId());
							      	}catch(Exception $e){
							      		$errorStack[]='Error in '.__FILE__.' '.$e->getMessage();
							      	}

							      	$responseAuthor = new Member();
							        try{
							          	 
								          echo 
								          '<div class="col-12 m-1">
								                <div class="card'.(($comment->getResponseTo()!=-1&&$comment->getResponseTo()==null) ? ' bg-light text-dark' : ' bg-dark text-white').'">
								                  <div class="card-body">
								                 
								                    <h6 class="card-title"><span class="badge">Ecrit par <a class="" href="/member?name='. strtolower($commentAuthor->getFirstname()) .'_'.strtolower($commentAuthor->getLastname()).'&id='. $commentAuthor->getId() .'"><img class="rounded m-1" src="../_assets/images/default_pp.jpg" width="25px" height="25px">'.ucfirst($commentAuthor->getFirstname()).' '.ucfirst($commentAuthor->getLastname()).'</a></span><span class="badge"> le '.$comment->getDate().'</span>
												        ';
												        if(isset($member)&&$comment->getAuthorId()==$member->getId()){
												        echo '
												        <a class="dropdown-toggle badge m-2" href="#" id="commentAction" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												          Options Auteur 
												        </a>
										                    <div class="dropdown-menu" aria-labelledby="commentAction">
										                    	<form method="post">
										                    		<input type="hidden" name="commentId" value="'.$comment->getId().'">
										                    		<input type="submit" name="modifyComment" class="dropdown-item text-capitalize  text-primary" value="Modifier">
										                    		<input type="submit" name="deleteComment" class="dropdown-item text-capitalize  text-danger" value="Supprimer">
										                    	</form>
										                    </div>';
									                	}
									                    echo '
									                    <span class="badge badge-pill badge-warning">Score du Commentaire : '.$comment->getScore().'</span>
								                    </h6>
								                    
								                    <p class="card-text">'.$comment->getContent().'</p>
								                    <form class="" method="post">
									                    <div class="" id="response'.$comment->getId().'">
									                    	<input type="hidden" name="parentCommentId" value="'.$comment->getId().'">
									                    	<input type="hidden" name="commentId" value="'.$comment->getId().'">';
											            	echo 
											            	'<button type="submit" class="  btn btn-success btn-sm m-1 p-1" style="width: 30px;text-align: center;padding: 6px 0;font-size: 17px;line-height: 1.428571429;border-radius:9px;" name="plusComment"><b>+</b></button>
														    <button type="submit" class="  btn btn-danger btn-sm m-1 p-1" style="width: 30px;text-align: center;padding: 6px 0;font-size: 17px;line-height: 1.428571429;border-radius:9px;" name="moinsComment"><b>-</b></button>';
											            	
											            echo '</div>
										            </form>
								                  </div>
								                </div>
								          </div>';
								          echo '<!-- Modal -->
												<div class="modal fade" id="reportCommentModal'.$comment->getId().'" tabindex="-1" role="dialog" aria-labelledby="reportCommentModalLabel'.$comment->getId().'" aria-hidden="true">
												  <div class="modal-dialog" role="document">
												    <div class="modal-content">
												      <div class="modal-header">
												        <h5 class="modal-title" id="reportCommentModalLabel'.$comment->getId().'">Demande de confirmation</h5>
												        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
												          <span aria-hidden="true">&times;</span>
												        </button>
												      </div>
												      <div class="modal-body">
												        Voulez vous vraiment signaler ce commentaire ?
												      </div>
												      <div class="modal-footer">
												        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Fermer</button>
												        <form method="post">
												        	<input type="hidden" name="commentId" value="'.$comment->getId().'">
												       	 	<input type="submit" class="btn btn-primary btn-sm" name="reportComment" value="Signaler">
												       	</form>
												      </div>
												    </div>
												  </div>
												</div>';
								      	 
							        }catch(Exception $e){
							        	$errorStack[]='Error in FILE: '.$e->getTrace()[0]['file'].' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
							        }
					        }
					    }else{
					    	echo '<h1 class="text-center">Aucun Commentaire(s)</h1>';
					    }
				    ?>

				 </div>
				</div>	
				<div class="card my-5">
				<h5 class="card-header text-uppercase bg-info text-white text-center">HISTORIQUE DE MES VUES</h5>
				 <div style="border:2px solid black;width:100%;height:400px;line-height:3em;overflow:auto;padding:5px;">
				 	
				 	<?php
				 		if(isset($views)){
					 		foreach ($views as $view) {
					 			$article = new Article();
					 			$articleAuthor = new Member();
					 			try{
									$article->setArticleById($view->getArticleId());
					 			}catch(UnavailableElementException $e){
					 				$article=NULL;
					 				$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
					 			}

					 			try{
					 				$articleViews=View::getViewsByArticleId($article->getId());
					 			}catch(UnavailableElementException $e){
					 				$articleViews=NULL;
					 				$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
					 			}

					 			try{
									$articleComments=Comment::getAllCommentsByArticleId($article->getId());
					 			}catch(UnavailableElementException $e){
					 				$articleComments=NULL;
					 				$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
					 			}


					 			try{
									$articleAuthor->setMemberById($article->getAuthorId());
					 			}catch(UnavailableElementException $e){
					 				$articleAuthor=NULL;
					 				$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
					 			}

								echo 
								'<div class="card my-1">
					                  <h5 class="card-header text-uppercase bg-dark text-white">Article vue le : '.$view->getDate().'</h5>
					                  <div class="card-body">
					                    <h5 class="card-title">'.$article->getTitle().'</h5>
					                    <span class="badge badge-primary ml-1">Vues : '.(isset($articleViews)&&!empty($articleViews) ? $articleViews->count():'0' ).'</span>
					                    <span class="badge badge-secondary ml-1">Commentaires : '.(isset($articleComments)&&!empty($articleComments) ? $articleComments->count():'0' ).'</span>
					                    <p class="bg-light col-12 p-2"><span class="badge">Ecrit par <img class="rounded m-1" src="../_assets/images/default_pp.jpg" width="30px" height="30px"><a href="/member?id='.$articleAuthor->getId().'">'.(isset($articleAuthor)&&!empty($articleAuthor)? ucfirst($articleAuthor->getFirstname()).' '.strtoupper($articleAuthor->getLastname()):'[ERROR]' ).'</a>   le '.$article->getDate().'</span></p>
					                    <a href="/article?action=read&title='.$article->getTitle().'&amp;id='.$article->getId().'" class="btn btn-secondary">Voir plus »</a>
					                  </div>  
					            </div>';
					        }
					    }else{
					    	echo '<h1 class="text-center">Aucune Vue(s)</h1>';
					    }
				    ?>

				 </div>
				</div>											           		            
			</div>
	</div>
</div>
</body>
</html>