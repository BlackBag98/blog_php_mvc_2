<html>
<?php include_once 'statics/head_view.php'; ?>
<body>
<?php include_once 'statics/header_view.php'; ?>
<div class="container">
	<div class="row">
			<div class="col-md-4 col-sm-12 bg-dark rounded m-1">
				<span class="rounded text-white badge badge-secondary m-2"><img class="rounded m-1 mr-4" src="../_assets/images/default_pp.jpg" width="50px" height="50px"><span class="mr-4" style="font-size:130%;"><?php echo ''.(isset($pageMember)&&!empty($pageMember) ? ''.ucfirst($pageMember->getFirstname()).' '.strtoupper($pageMember->getLastname()):'0' ); ?></span></span>
				<form method="post">
					<?php
						if(isset($_SESSION['member'])){
							$member = unserialize($_SESSION['member']);
							$subscription = new Subscription();
							try{
								$subscription->setSubscriptionBySubscriberAndAuthorId($member->getId(),$_GET['id']);
								echo '<input type="submit" class="btn btn-danger btn-sm m-1" name="unsubscribeAuthor" value="Se desabonner">';
							}catch(UnavailableElementException $e){
								echo '<input type="submit" class="btn btn-warning btn-sm m-1" name="subscribeAuthor" value="S\'abonner">';
							}
						}else{
							echo '<input type="submit" class="btn btn-warning btn-sm m-1" name="subscribeAuthor" value="S\'abonner">';
						}
					?>
				</form>
				<div class="card bg-secondary my-1">
	                  <h5 class="card-header text-uppercase bg-dark text-white">Informations :</h5>
	                  <div class="card-body">
	                    <h5>
	                    	<span class="badge badge-success m-1">Abonnees : <?php echo ''.(isset($subscriptions)&&!empty($subscriptions) ? $subscriptions->count():'0' ); ?></span>
	                    	<span class="badge badge-dark m-1">Article(s) Ecrit: <?php echo ''.(isset($articles)&&!empty($articles) ? $articles->count():'0' ); ?></span>
	                    	<span class="badge badge-primary m-1">Vue(s) Article(s): <?php echo ''.$countAllViews; ?></span>
	                    	<span class="badge badge-info m-1">Commentaire(s) Ecrit(s): <?php echo ''.(isset($comments)&&!empty($comments) ? $comments->count():'0' ); ?></span>
	                    	<span class="badge badge-danger m-1">Role : <?php echo ''.(isset($role)&&!empty($role) ? ucfirst($role->getName()):'PRIVILEGE_ERROR' ); ?></span>
	                    	<span class="badge badge-warning m-1">Totales Score Commentaire(s): <?php echo ''.(isset($score)&&!empty($score) ? $score :'0' ); ?></span>
	                    </h5>
	                  </div>  
	            </div>
	        </div>
			<div class="col-md-7 col-sm-12 bg-warning">
				 <div style="width:100%;height:1000px;line-height:3em;overflow:auto;padding:5px;">

				 	<?php
				 		if(isset($articles)){
					 		foreach ($articles as $article) {
					 			# code...
					 			try{
					 				$articleViews=View::getViewsByArticleId($article->getId());
					 			}catch(UnavailableElementException $e){
					 				$articleViews=NULL;
					 				$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
					 			}

					 			try{
									$articleComments=Comment::getAllCommentsByArticleId($article->getId());
					 			}catch(UnavailableElementException $e){
					 				$articleComments=NULL;
					 				$errorStack[]='Error in FILE: '.__FILE__.' LINE: '.$e->getTrace()[0]['line'].' MESSAGE: '.$e->getMessage();
					 			}
								echo 
								'<div class="card my-1">
					                  <h5 class="card-header text-uppercase bg-dark text-white">ARTICLE</h5>
					                  <div class="card-body">
					                    <h5 class="card-title">'.$article->getTitle().'</h5>
					                    <span class="badge badge-primary ml-1">Vues : '.(isset($articleViews)&&!empty($articleViews) ? $articleViews->count():'0' ).'</span>
					                    <span class="badge badge-secondary ml-1">Commentaires : '.(isset($articleComments)&&!empty($articleComments) ? $articleComments->count():'0' ).'</span>
					                    <p class="bg-light col-12 p-2"><span class="badge">Ecrit le '.$article->getDate().'</span></p>
					                    <p class="card-text">'.$article->getSentence().'</p>
					                    <a href="/article?action=read&title='.$article->getTitle().'&amp;id='.$article->getId().'" class="btn btn-secondary">Voir plus »</a>
					                  </div>  
					            </div>';
					        }
					    }else{
					    	echo '<h1 class="text-center">Aucun Article(s)</h1>';
					    }
				    ?>

				 </div> 	            		            
			</div>
	</div>
</div>
</body>
</html>