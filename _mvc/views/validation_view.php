<html>
<?php include_once 'statics/head_view.php'; ?>
<body>
<?php include_once 'statics/header_view.php'; ?>

  <div class="container">
  </div>
    <div class="container">
    <main role="main" class="container">

  <noscript>
    <div class="container">
      <div class="alert alert-warning m-3">
        Le javascript est desactive la gestion des erreurs d'insertion se fera donc par le serveur et sera par consequent plus lente.<br/>
        Activer le javascript pour une meilleur experience utilisateur.
      </div><center></center>				<center></center>
    </div>
  </noscript>
      <?php
      if (isset($validation_success)){
          echo '<div class="alert alert-success m-2" role="alert">'.$validation_success.'</div>';
      }else{
        
       echo   '<div class="row">
                <div class="col-sm-6 offset-sm-3 text-center">
                    <div class="display-4">Voulez vous renvoyer un lien d\'activation ?</div>
                    <div id="after" class="info-form m-3">
                        <form id="validation-form" action="/validation" method="post" class="form-inlin justify-content-center">
                            <div class="form-group">
                                <label class="sr-only">Email</label>
                                <input type="text" name="email" id="formControlEmailInput1" class="form-control" placeholder="name@test.com">
                            </div>
                            <button type="submit" name="btnSendNewActivationLink" class="btn btn-success ">Envoyer</button>
                        </form>
                    </div>
                    <br>
                </div>
            </div>';

      }
      if(isset($validation_error))
      {
          echo '<div class="alert alert-danger m-2" role="alert">'.$validation_error.'</div>';
      }
    ?>
    </main><!-- /.container -->
  </div>
</body>                     
</html>